<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
     <title>React</title>
     <script src="https://npmcdn.com/react@15.3.0/dist/react.js"></script>
     <script src="https://npmcdn.com/react-dom@15.3.0/dist/react-dom.js"></script>
     <script src="https://npmcdn.com/babel-core@5.8.38/browser.min.js"></script>
     <script src="https://npmcdn.com/jquery@3.1.0/dist/jquery.min.js"></script>
     <script src="https://npmcdn.com/remarkable@1.6.2/dist/remarkable.min.js"></script>
    </head>
    <body>
    <div id="root"></div>

    <script type="text/babel">
        class LoginForm extends React.Component {
            constructor(props) {
                super(props);
                this.state = {firstname: '', lastname: '', email: '', version: ''};

                this.onFirstnameChange = this.onFirstnameChange.bind(this);
                this.onLastnameChange = this.onLastnameChange.bind(this);
                this.onEmailChange = this.onEmailChange.bind(this);
                this.onVersionChange = this.onVersionChange.bind(this);
                this.onSubmit = this.onSubmit.bind(this);
            }

            onSubmit(event){
                alert(`${this.state.firstname}, добро пожаловать!`);
                event.preventDefault();
            }

            onFirstnameChange(event){
                this.setState({firstname: event.target.value});
            }

            onLastnameChange(event) {
                this.setState({lastname: event.target.value});
            }
            onEmailChange(event) {
                this.setState({email: event.target.value});
            }
            onVersionChange(event) {
                this.setState({version: event.target.value});
            }

            render() {
                return (

                    <form method="POST" action="http://lara3field.loc/api/page">
                        <p><label> Firstname: <input type="text" name="firstname" value={this.state.firstname}
                                                 onChange={this.onFirstnameChange}/></label></p>
                        <p><label> Lastname: <input type="text" name="lastname" value={this.state.lastname}
                                                  onChange={this.onLastnameChange}/></label></p>
                        <p><label> Email: <input type="email" name="email" value={this.state.email}
                                                 onChange={this.onEmailChange} placeholder="1243123"/></label></p>
                        <p><label> Version: <input type="text" name="version" value={this.state.version}
                                                 onChange={this.onVersionChange}/></label></p>

                        <p><input type="submit" value="Deploy" /></p>
                    </form>


                );
            }
        }

        ReactDOM.render(<LoginForm />,  document.getElementById('root'));


    </script>
    <br>
    <hr>
    <br>
    <br>
    <br>


<?php

$handle= fopen('/home/anatolii/sites/testreact.loc/test_123.csv', 'r');
$i=0;
while ($i<2) {

    $pointer = trim(str_replace('"','',fgets($handle)));
    $homepage = file_get_contents($pointer);


    if(empty($homepage)){
//        echo "nonono"."<br>".$pointer;

        $custom_urls_file_path = '/home/anatolii/sites/testreact.loc/123.csv';

        $fp = fopen($custom_urls_file_path, 'w');

            fwrite($fp, $pointer,PHP_EOL);

    }else{
        echo ":gotovo";
        die();
    }
    $i++;
    fclose($fp);

}

?>


    </body>
    </html>